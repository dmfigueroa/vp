/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vp;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author David
 */
public class VP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese número primo: ");
        BigInteger primo = sc.nextBigInteger();
        System.out.println((isPrime(primo)) ? "Es primo" : "No es primo");
    }

    public static boolean isPrime(BigInteger num) {
        BigInteger dos = new BigInteger("2");
        if (num.compareTo(dos) > 0 && num.mod(dos).compareTo(BigInteger.ZERO) == 0) {
            return false;
        }
        BigInteger top = sqrt(num).add(BigInteger.ONE);
        BigInteger i = dos.add(BigInteger.ONE);
        while (i.compareTo(top) < 0) {
            if (num.remainder(i).compareTo(BigInteger.ZERO) == 0) {
                return false;
            }
            i = i.add(dos);
        }
        return true;
    }

    private static BigInteger sqrt(BigInteger num) {
        BigInteger a = BigInteger.ONE;
        BigInteger b = new BigInteger(num.shiftRight(5).add(new BigInteger("8")).toString());
        while (b.compareTo(a) >= 0) {
            BigInteger mid = new BigInteger(a.add(b).shiftRight(1).toString());
            if (mid.multiply(mid).compareTo(num) > 0) {
                b = mid.subtract(BigInteger.ONE);
            } else {
                a = mid.add(BigInteger.ONE);
            }
        }
        return a.subtract(BigInteger.ONE);
    }

}
